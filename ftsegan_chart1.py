import pandas as pd,numpy as np,matplotlib.pyplot as plt, seaborn as sns

def generate_preds(num_sims_wanted,historical_data,future_data):      
    future_data=future_data.set_index('date')
    fda=np.array(future_data)
    selected_predictions=np.random.choice(range(fda.shape[1]),size=num_sims_wanted)
    #by default, the selection is without replacement so you can choose as many as you like
    future_preds=pd.DataFrame(data=fda[:,selected_predictions],index=future_data.index)
    mindate=pd.to_datetime('2020-02-01')
    past_and_future=pd.concat([historical_data[['eomcl']],future_preds],axis=0)
    pafplot=past_and_future[past_and_future.index>=mindate]
    plt.xticks(rotation=90)
    sns.lineplot(data=pafplot,legend=False)\
        .set(title='FTSE All Share GAN - '+str(num_sims_wanted)+' Simulations')
    plt.savefig(dr+'ftsegan_data_'+str(num_sims_wanted)+'.png',bbox_inches='tight')
    plt.show()
    plt.clf()
    return future_preds

dr='/Users/paulpapenfus/devgoals/tg2/'
hist,fut='ftas_m.csv','fut_10000.csv'
sns.set_style("darkgrid")
sns.set(rc={'axes.facecolor':'seashell','figure.dpi':150,'savefig.dpi':150})
historical_data=pd.read_csv(dr+hist,parse_dates=['close_date_eom'],dayfirst=True)
historical_data=historical_data.set_index('close_date_eom')
future_data=pd.read_csv(dr+fut,parse_dates=['date'],dayfirst=True)

#generate_preds(150,historical_data,future_data)
