# About

This repo is a very simplistic interpretation to demonstrate a few snippets from my personal research. It is in no way complete, because separate components are needed for separate applications, and is offered with no guarantees of any kind. In particular, the simulations rely on hosting a model in the cloud. The model has been disconnected due to the ongoing subscription fees.

The front end is still available at https://paulsftsegan.netlify.app/ but the SimulateMe! button, which generated the desired number of future investment return scenarios, will no longer work.

For the avoidance of doubt, nothing in this work should be interpreted as financial advice.

# Background

A Generative Adversarial Network (GAN) is a machine learning framework where two Artificial Neural Networks (ANNs), a generator and a discriminator, play a game against each other. The game focuses on a certain topic such as images of human faces. The generator has never seen what a human face looks like, but tries to make up images that might fool the discriminator. The discriminator does have access to a collection of human faces and tells the discriminator if the generated images look fake or real. The generator uses the feedback to imagine more realistic-looking images. The discriminator also gets better at telling what is real as it tries to do better than the generator. The game continues for many rounds until the discriminator can no longer tell with confidence whether an image is real or fake.

When the game reaches this stage, it is possible to use the generator to simulate some very realistic-looking faces. For some examples generated in this way, visit https://thispersondoesnotexist.com

# ftsegan

GANs do not work only on images. They can also be used to simulate time series data. This repo is a very simple example where the discriminator has access to historical returns from the FTSE All Share Index. The generator provides return data that becomes so realistic that the discriminator cannot tell whether it is real or not.

There are many ways to simulate what future investment returns might look like. The GAN method adds an important property to the existing tools: the GAN simulations are similar enough to historical returns that a computer cannot tell the difference.

## Design and Structure

In one way or another, many software projects rely on the work of others. Apart from researching and finding the right inputs, my work here focused on adapting an existing GAN codebase, designed to work on Tensorflow, training it on historical returns from the FTSE All Share Index, hosting the results on AWS using Lambda and adapting a basic front end using React and Netlify. This repo shows some of the key concepts from my own work, developed over many iterations of trial and error. The supporting imports are available from the Resources below.

## Resources

GAN implementation on human faces
https://thispersondoesnotexist.com/

Python implementation of time series GANs
https://github.com/ydataai/ydata-synthetic

Implementation of a GAN on image data
https://github.com/ahmedbesbes/cartoonify

Article discussing the main concepts
https://pub.towardsai.net/gans-for-synthetic-data-generation-1cca50317d87

Basic FTSE simulator
https://paulsftsegan.netlify.app/
