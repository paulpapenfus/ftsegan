import React, { Component } from 'react';
import logo from './cball.jpg';
import './App.css';
import {useState} from 'react';
import { transform } from './api.js'

const App = () => {
  const [numsims, setNumsims] = useState('');
  const downloadFile = ({ data, fileName, fileType }) => {
      const blob = new Blob([data], { type: fileType })
      const a = document.createElement('a')
      a.download = fileName
      a.href = window.URL.createObjectURL(blob)
      const clickEvt = new MouseEvent('click', {
        view: window,
        bubbles: true,
        cancelable: true,
      })
      a.dispatchEvent(clickEvt)
      a.remove()
  }
  
  var handleChange = (event) => {
    setNumsims(event.target.value);
    console.log('number of sims is:', numsims);
  };
  
  var handleClick = (event) => {
        event.preventDefault();
        console.log('posting: ',numsims)
        let data = {
                  num_sims_wanted: numsims,
                };
                transform(data)
                  .then((response) => {
                    console.log("success");
                    let out = response.data.output
                    console.log(out);
                    downloadFile({
                      data: JSON.stringify(out),
                      fileName: 'sims_'+numsims+'.json',
                      fileType: 'text/json',
                    });                  
                  })
                  .catch((response) => {
                    console.log(response);
                  });
    }

  return (
    <div className="App">
      <Header />
      <Intro />
      <h3>
      <p>How many versions of the future would you like?</p>
      </h3>
      <input
        type="text"
        id="value"
        name="value"
        onChange={handleChange}
        value={numsims}
        autoComplete="off"
      />
      <button onClick={handleClick}>SimulateMe!</button>
      <p></p>
      <div>
    </div>
      <p></p>
    </div>
  );
};

class Header extends Component {
  render(){
   return(
    <div className="App-header">
      {
      <img src={logo} className="App-logo" alt="logo" />
      }
      <h2>FTSE All Share Index GAN Simulator</h2>
    </div>
    
    );
   }
}

class Intro extends Component {
  render(){
   return(
    <div>
    <h2>Background</h2>
    <p className="App-intro">
      A Generative Adversarial Network (GAN) is a machine learning framework where two neural networks, a generator and a discriminator, play a game against each other.
      The game focuses on a certain topic such as images of human faces. 
      The generator has never seen what a human face looks like, but tries to make up images that might fool the discriminator.
      The discriminator does have access to a collection of human faces and tells the generator if the generated images look fake or real.
      The generator uses the feedback to imagine more realistic-looking images. 
      The discriminator also gets better at telling what is real as it tries to do better than the generator.
      The game continues for many rounds until the discriminator can no longer tell with confidence whether an image is real or fake.
    </p>
    <p>
    When the game reaches this stage, it is possible to use the generator to simulate some very realistic-looking faces.
    For some examples generated in this way, visit thispersondoesnotexist.com
    </p>
    <h2>Welcome to the FTSE GAN</h2>
    <p>
      GANs do not work only on images. They can also be used to simulate time series data. 
      In this example, the discriminator has access to historical returns from the FTSE All Share Index.
      The generator provides return data that becomes so realistic that the discriminator cannot tell whether it is real or not.  
    </p>
    <p>
    There are many ways to simulate what future investment returns might look like.
    The GAN method adds an important property to the existing tools: the GAN simulations are similar enough to historical returns that a computer cannot tell the difference.
    </p>
    </div>
    );
   }
}

export default App;

