import pandas as pd,numpy as np#,os
from sklearn.preprocessing import StandardScaler
from joblib import load
import seaborn as sns
from collections import namedtuple
import matplotlib.pyplot as plt
from ydata_synthetic.synthesizers.timeseries.timegan.model import TimeGAN

sns.set_style("darkgrid")
sns.set(rc={'axes.facecolor':'seashell','figure.dpi':150,'savefig.dpi':150})

_model_parameters = ['batch_size', 'lr', 'betas', 'layers_dim', 'noise_dim',
                     'n_cols', 'seq_len', 'condition', 'n_critic', 'n_features', 'tau_gs']
_model_parameters_df = [128, 1e-4, (None, None), 128, 264,
                        None, None, None, 1, None, 0.2]
ModelParameters = namedtuple('ModelParameters', _model_parameters, defaults=_model_parameters_df)


def initialise_model():
    #Specific to TimeGANs
    seq_len=24
    n_seq = 1    
    noise_dim = 32
    dim = 128
    batch_size = 128
    learning_rate = 5e-4
    
    gan_args = ModelParameters(batch_size=batch_size,
                               lr=learning_rate,
                               noise_dim=noise_dim,
                               layers_dim=dim)
    
    synth=TimeGAN(model_parameters=gan_args,hidden_dim=24,seq_len=seq_len,n_seq=n_seq,gamma=1)
    return synth
    
def load_model(synth,dr,fn):
    synth=synth.load(dr+fn)
    return synth

def generate_preds(num_sims_wanted):
    d=pd.read_csv(dr+'ftas_m.csv',parse_dates=['close_date_eom'],dayfirst=True)
    final_close=d.eomcl.iloc[-1]
    #this is 4107, the number on 29 July 2022, the last eomclose that the model returns were calculated from for training
    #so the last closing datapoint is July, formatted 1 July 2022, and the first predition model point
    
    final_index_dt=d.close_date_eom.iloc[-1]
    future_dates=pd.date_range(final_index_dt,periods=25,freq='M')[1:]
    d=d.set_index('close_date_eom')
    
    synth=initialise_model()
    synth=load_model(synth,dr,'ftsegan1.pkl')
    preds_sc=synth.sample(num_sims_wanted)[:num_sims_wanted,:,:]
    
    scaler=StandardScaler()
    scaler=load(dr+'std_scaler.bin')
    sh=preds_sc.shape
    preds=np.empty(sh)
    for i in range(sh[0]):
        preds[i]=scaler.inverse_transform(preds_sc[i])
    preds=preds[:,:,0]
    
    sh=preds.shape
    pred_indx=np.empty(sh)
    for i in range(sh[0]):
        pred_indx[i][0]=final_close*(1+preds[i][0])
        for j in range(1,sh[1]):
            pred_indx[i][j]=pred_indx[i][j-1]*(1+preds[i][j])
        
    future_preds=pd.DataFrame(data=pred_indx.T,index=future_dates)
    past_and_future=pd.concat([d[['eomcl']],future_preds],axis=0)
    mindate=pd.to_datetime('2020-02-01')
    pafplot=past_and_future[past_and_future.index>=mindate]
    plt.xticks(rotation=90)
    sns.lineplot(data=pafplot,legend=False)\
        .set(title='FTSE All Share GAN - '+str(num_sims_wanted)+' Simulations')
    plt.savefig(dr+'ftsegan_pkl_'+str(num_sims_wanted)+'.png',bbox_inches='tight')
    plt.show()
    plt.clf()
    return future_preds

#f=generate_preds(150)