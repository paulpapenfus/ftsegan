from os import path
import pandas as pd,numpy as np,os
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from ydata_synthetic.synthesizers import ModelParameters
from ydata_synthetic.synthesizers.timeseries import TimeGAN
from joblib import dump

dr='/Users/paulpapenfus/devgoals/tg1/'
os.chdir(dr)
d=pd.read_csv(dr+'ftas_m.csv',parse_dates=['date'],dayfirst=True)
d=d.set_index('date')
d=d[['rtn']]
scaler=StandardScaler()
scaler.fit(np.array(d))
d_t=scaler.transform(d)
dump(scaler,dr+'std_scaler.bin',compress=True)

#Specific to TimeGANs
seq_len=24
n_seq = 1
hidden_dim=24
gamma=1

noise_dim = 32
dim = 128
batch_size = 128

log_step = 100
learning_rate = 5e-4

def sequence_and_shuffle(ori_data,seq_len):
    # Preprocess the dataset
    temp_data = []
    # Cut data by sequence length
    for i in range(0, len(ori_data)-seq_len):
        _x = ori_data[i:i + seq_len]
        temp_data.append(_x)

    # Mix the datasets (to make it similar to i.i.d)
    idx = np.random.permutation(len(temp_data))
    data = []
    for i in range(len(temp_data)):
        data.append(temp_data[idx[i]])
    return data

stock_data=sequence_and_shuffle(d_t,24)

gan_args = ModelParameters(batch_size=batch_size,
                           lr=learning_rate,
                           noise_dim=noise_dim,
                           layers_dim=dim)

synth=TimeGAN(model_parameters=gan_args,hidden_dim=24,seq_len=seq_len,n_seq=n_seq,gamma=1)
synth.train(stock_data,train_steps=50000)
synth.save(dr+'ftsegan1.pkl')
